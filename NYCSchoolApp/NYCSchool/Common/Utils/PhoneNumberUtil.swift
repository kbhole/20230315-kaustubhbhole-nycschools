//
//  PhoneNumberUtil.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation
import SwiftUI

struct PhoneNumberUtil {

    static func dial(phoneNumber: String) {
        let phone = "tel://"
        let phoneNumberformatted = phone + phoneNumber
        guard let url = URL(string: phoneNumberformatted) else {
            GlobalLogger.sharedLogger.send("Please check Phone Number", logLevel: .error)
            return
        }
        UIApplication.shared.open(url)
    }
}
