//
//  Constants.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation

struct Constants {
    static let standardHorizontalPadding = 16.0
    static let standardVerticalPadding = 12.0
}
