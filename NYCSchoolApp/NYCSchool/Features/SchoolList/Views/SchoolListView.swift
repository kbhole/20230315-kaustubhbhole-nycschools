import SwiftUI

struct SchoolListView: View {

    // MARK: Variables -
    @ObservedObject var viewModel: SchoolListViewModel
    // MARK: View Body -
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.schools, id: \.dbn) { school in
                    ZStack {
                        RoundedRectCardComponent {
                            AnyView(getListContentView(school: school))
                        }
                        NavigationLink(destination: SchoolDetailsView(viewModel: viewModel.getSchoolDetailsViewModel(from: school))) {
                            EmptyView()
                        }
                        .opacity(.zero)
                    }
                    .listRowSeparator(.hidden)
                }
            }
            .listStyle(PlainListStyle())
            .frame(maxWidth: .infinity)
            .navigationTitle(LocalizableStrings.screenTitle.description)
            .onLoad {
                viewModel.fetchNYCSchoolsFromAPI { isSuccess in
                    GlobalLogger.sharedLogger.send("Fetch School API Success", logLevel: .info)
                }
            }
        }
    }

    /// Get List Content
    /// - Parameter school: school object
    /// - Returns: Return List Cell View
    func getListContentView(school: SchoolViewModel) -> any View {
        VStack (alignment: .leading) {
            Text(school.schoolName)
                .font(Font.titleSmall)
                .foregroundColor(.neutralDarkClay)
            SecondaryTextView(text: school.email)
            PrimaryTextView(text: school.address)
        }
    }
}
