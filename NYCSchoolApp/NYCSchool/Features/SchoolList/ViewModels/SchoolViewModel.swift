//
//  SchoolViewModel.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation

/// Struct which holds Data to be display on the Screen
struct SchoolViewModel {

    // MARK: Variables -
    var schoolName: String
    var email: String
    var website: String
    var phone: String
    var address: String
    var dbn: String
    var latitude: String
    var longitude: String
    var overviewParagraph: String
    var schoolSATData: SchoolSATData?
}
