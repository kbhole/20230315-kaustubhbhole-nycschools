//
//  WebRepository.swift
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation
import Combine

/// Protocol to Confirm for all WebServices Classes
public protocol WebServiceProtocol {
    /// Base Url for the APis
    var baseURL: String { get }
}

/// Extension for WebServiceProtocol
public extension WebServiceProtocol {

    /// Session Object for Api calls
    var session: URLSession {
        return .shared
    }

    /// Generic Methods for calling API from all WebServices classes. Call this method when Api returns Decodable Object
    /// - Parameters:
    ///   - endpoint: endpoint
    ///   - httpCodes: httpCodes
    /// - Returns: Publisher
    func callApi<Value>(endpoint: URlRequestBuilder,
                               httpCodes: HTTPCodes = .success) -> AnyPublisher<Value, Error>
        where Value: Decodable {
        do {
            let request = try endpoint.urlRequest(baseURL: baseURL)
            return session
                .dataTaskPublisher(for: request)
                .decodeResponse(httpCodes: httpCodes)
        } catch let error {
            return Fail<Value, Error>(error: error).eraseToAnyPublisher()
        }
    }

    /// Generic Methods for calling API from all WebServices classes. Call this method when Api returns no Response But Status Code.
    /// - Parameters:
    ///   - endpoint: endpoint
    ///   - httpCodes: httpCodes
    /// - Returns: Publisher
    func callApi(endpoint: URlRequestBuilder,
                                    httpCodes: HTTPCodes = .success) -> AnyPublisher<Void, Error> {
        do {
            let request = try endpoint.urlRequest(baseURL: baseURL)
            return session
                .dataTaskPublisher(for: request)
                .tryMap {
                    guard let code = ($0.1 as? HTTPURLResponse)?.statusCode else {
                        throw APIError.unexpectedResponse
                    }
                    guard httpCodes.contains(code) else {
                        throw APIError.httpCode(code)
                    }
                    return Void()
                }
                .extractUnderlyingError()
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()

        } catch let error {
            return Fail<Void, Error>(error: error).eraseToAnyPublisher()
        }
    }
}

// MARK: - Helpers
/// Extension for Publisher to add some new functionality
private extension Publisher where Output == URLSession.DataTaskPublisher.Output {

    /// Function to Decode Response
    /// - Parameter httpCodes: httpCodes received
    /// - Returns: Any Publisher
    func decodeResponse<Value>(httpCodes: HTTPCodes) -> AnyPublisher<Value, Error> where Value: Decodable {
        return tryMap {
                assert(!Thread.isMainThread)
                guard let code = ($0.1 as? HTTPURLResponse)?.statusCode else {
                    throw APIError.unexpectedResponse
                }
                guard httpCodes.contains(code) else {
                    throw APIError.httpCode(code)
                }
                return $0.0
            }
            .extractUnderlyingError()
            .decode(type: Value.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}

/// Extension for Error
private extension Error {

    /// Underlying Error
    var underlyingError: Error? {
        let nsError = self as NSError
        if nsError.domain == NSURLErrorDomain && nsError.code == -1009 {
            // "The Internet connection appears to be offline."
            return self
        }
        return nsError.userInfo[NSUnderlyingErrorKey] as? Error
    }
}

/// Extract Error
extension Publisher {

    /// Function to Extract Underlying Error
    /// - Returns: Error
    func extractUnderlyingError() -> Publishers.MapError<Self, Failure> {
        mapError {
            ($0.underlyingError as? Failure) ?? $0
        }
    }
}
