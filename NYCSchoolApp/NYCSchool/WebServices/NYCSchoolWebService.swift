//
//  TestWebService.swift
//  Core_Example
//
//  Created by Bhole, Kaustubh Satish on 27/07/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Combine
import Foundation

/// Health History Repository protocol
protocol NYCSchoolWebServiceProtocol: WebServiceProtocol {
    /// Method to fetch Heath History from Apis
    func fetchNYCSchools() -> AnyPublisher<[School], Error>
    func fetchSchoolSATDetails(dbn: String) -> AnyPublisher<[SchoolSATData], Error>

    var baseURL: String { get }
}

/// Health History Web Repository
struct NYCSchoolWebService: NYCSchoolWebServiceProtocol {
    var baseURL = "https://data.cityofnewyork.us/resource"

    /// List of all Apis related to Health History Module
    enum API {
        case fetchNYCSchools
        case fetchSchoolSATDetails(dbn: String)
    }

    /// Call API to fetch Health History
    /// - Returns: Health History Object
    func fetchNYCSchools() -> AnyPublisher<[School], Error> {
        return callApi(endpoint: API.fetchNYCSchools)
    }

    func fetchSchoolSATDetails(dbn: String) -> AnyPublisher<[SchoolSATData], Error> {
        return callApi(endpoint: API.fetchSchoolSATDetails(dbn: dbn))
    }
}

/// List of all Api Urls end Points
enum ApiEndPoints: String {

    // End point for Health History Api
    case fetchNYCSchools = "/s3k6-pzi2.json?$limit=20"
    case fetchSchoolSATDetails = "/f9bf-2cp4.json"
}


// Extension for creating URLRequest
extension NYCSchoolWebService.API: URlRequestBuilder {
    var endPoint: String {
        switch self {
        case .fetchNYCSchools:
            return ApiEndPoints.fetchNYCSchools.rawValue
        case .fetchSchoolSATDetails(let dbn):
            return ApiEndPoints.fetchSchoolSATDetails.rawValue + "?dbn=\(dbn)"
        }
    }

    /// HTTP Method Type POST, GET etc
    var method: String {
        switch self {
        case .fetchNYCSchools, .fetchSchoolSATDetails:
            return "GET"
        }
    }

    /// Headers to pass along with Request
    var headers: [String: String]? {
        return ["X-App-Token": "udJEv3v0y4TEEllVAm0uIUiiI"]
    }

    /// For the POST Apis, Provide Custom body here. Use formUrlEncodedData() function to create Dictionary to Data.
    /// - Throws: Exception
    /// - Returns: Data
    func body() throws -> Data? {
        return nil
    }
}
